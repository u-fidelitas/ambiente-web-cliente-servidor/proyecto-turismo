<?php
include("database.php");
$con = OpenConnection();

if (isset($_POST['save'])) {
	// var_dump($_POST['Id']);
	// var_dump($_POST['Titulo']);
	// var_dump($_POST['LocalizacionId']);
	// var_dump($_POST['NombreContacto']);
	// var_dump($_POST['EmailContacto']);
	// var_dump($_POST['TelefonoContacto']);
	// var_dump($_POST['IdUsuario']);
	// var_dump($_POST['IdCategoria']);
	
	$Id = mysqli_real_escape_string($con, (strip_tags($_POST["Id"], ENT_QUOTES)));
	$Titulo = mysqli_real_escape_string($con, (strip_tags($_POST["Titulo"], ENT_QUOTES)));
	$Descripcion = mysqli_real_escape_string($con, (strip_tags($_POST["Descripcion"], ENT_QUOTES)));
	$LocalizacionId = mysqli_real_escape_string($con, (strip_tags($_POST["LocalizacionId"], ENT_QUOTES)));
	$NombreContacto = mysqli_real_escape_string($con, (strip_tags($_POST["NombreContacto"], ENT_QUOTES)));
	$EmailContacto = mysqli_real_escape_string($con, (strip_tags($_POST["EmailContacto"], ENT_QUOTES)));
	$TelefonoContacto	= mysqli_real_escape_string($con, (strip_tags($_POST["TelefonoContacto"], ENT_QUOTES)));
	$IdUsuario	= mysqli_real_escape_string($con, (strip_tags($_POST["IdUsuario"], ENT_QUOTES)));
	$IdCategoria	= mysqli_real_escape_string($con, (strip_tags($_POST["IdCategoria"], ENT_QUOTES)));

	$sql = "UPDATE Tours SET  Id=NULL, Titulo='$Titulo' , Descripcion='$Descripcion', LocalizacionId='$LocalizacionId', NombreContacto='$NombreContacto', EmailContacto='$EmailContacto', TelefonoContacto='$TelefonoContacto', IdUsuario='$IdUsuario', IdCategoria='$IdCategoria' WHERE Id='$Id'";
	$update = mysqli_query($con, $sql);
	CloseConnection($con);
	if ($update) {
		echo '<div class="primary alert-primary alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se guardaron los cambios con exito.</div>';
	} else {
		echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
	}
} else {
	echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
}
			?>
			<form class="form-horizontal" action="" method="post">
			
			<div class="form-group">
		<label class="col-sm-3 control-label">Id</label>
		<div class="col-sm-2">
			<input type="text" name="Id" class="form-control" placeholder="Id" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Título</label>
		<div class="col-sm-4">
			<input type="text" name="Titulo" value="" class="form-control" placeholder="Título" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Descripción</label>
		<div class="col-sm-4">
			<input type="text" name="Descripcion" value="" class="form-control" placeholder="Descripcion" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">LocalizacionId</label>
		<div class="col-sm-2">
			<input type="text" name="LocalizacionId" class="form-control" placeholder="LocalizacionId" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">NombreContacto</label>
		<div class="col-sm-4">
			<input type="text" name="NombreContacto" value="" class="form-control" placeholder="NombreContacto" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">EmailContacto</label>
		<div class="col-sm-3">
			<input type="text" name="EmailContacto" value="" class="form-control" placeholder="EmailContacto" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">TelefonoContacto</label>
		<div class="col-sm-2">
			<input type="text" name="TelefonoContacto" class="form-control" placeholder="TelefonoContacto" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">IdUsuario</label>
		<div class="col-sm-2">
			<input type="text" name="IdUsuario" class="form-control" placeholder="IdUsuario" required>
		</div>

		<div class="form-group">
		<label class="col-sm-3 control-label">IdCategoria</label>
		<div class="col-sm-4">
			<input type="text" name="IdCategoria" value="" class="form-control" placeholder="IdCategoria" required>
		</div>
	</div>

	</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">&nbsp;</label>
			<div class="col-sm-6">
				<input type="submit" name="add" class="btn btn-sm btn-primary" value="Guardar datos de los Tours">
				<a href="index.php" class="btn btn-sm btn-danger">Cancelar</a>
			</div>
		</div>    

				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="save" class="btn btn-sm btn-primary" value="Guardar datos de los Tours">
						<a href="index.php" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>