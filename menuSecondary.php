<nav class="navbar navbar-expand-lg navbar-dark fixed-top py-3" id="secondaryNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"> Adventure Costa Rica</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <?php
                include 'menuItems.php';
                ?>
            </ul>
        </div>
    </div>
</nav>