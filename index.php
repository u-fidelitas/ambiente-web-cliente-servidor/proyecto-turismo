<?php

include 'database.php';
$conn = OpenConnection();

CloseConnection($conn);

?>

<!DOCTYPE html>
<html lang="es">

<?php
include 'head.php';
?>

<body id="page-top">
    <!-- Navigation-->
    <?php
    include 'menu.php';
    ?>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-10 align-self-end">
                    <h1 class="text-uppercase text-white font-weight-bold">Adventure Costa Rica</h1>
                    <hr class="divider my-4" />
                </div>
                <div class="col-lg-8 align-self-baseline">
                    <p class="text-white-75 font-weight-light mb-5">Adventure Costa Rica es una agencia de viajes certificada y cualificada con la que disfrutarás de grandes momentos de aventura</p>
                    <p>Atrévete a explorar Costa Rica</p>
                </div>
            </div>
        </div>
    </header>
    <!-- About-->
    <section class="page-section bg-primary" id="about">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class="text-white mt-0">Qué es Adventure Costa Rica?</h2>
                    <hr class="divider light my-4" />
                    <p class="text-white-50 mb-4">Somos una agencia de viajes en Costa Rica que ofrece los mejores lugares turísticos para vacacionar.

                        Vive con nosotros la experiencia de las mejores aventuras de Costa Rica y comprueba las maravillas de la flora y fauna del turismo costarricense.

                    </p>
                    <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Listo para la aventura!</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <h2 class="text-center mt-0"> Servicios que brindamos</h2>
            <hr class="divider my-4" />
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="mt-5">
                        <!-- <i class="fas fa-4x fa-gem text-primary mb-4"></i>  "icono"-->
                        <h3 class="h4 mb-2">¿Dónde hospedarme?</h3>
                        <p class="text-muted mb-0">Para una mejor y mayor experiencia tenemos cabinas totalmente equipadas para que la puedan pasar pura vida!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="mt-5">
                        <!-- <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i> -->
                        <h3 class="h4 mb-2">¿Cómo movilizarme?</h3>
                        <p class="text-muted mb-0">Además de la experiencia de aventura que ofrecemos, tenemos un convenio con la empresa
                            National Car Rental, el cual consiste en que todos nuestros clientes que requieran de un vehículo
                            tendrán un descuento del 35%. Así que movilizate con seguridad, comodidad y economía.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="mt-5">
                        <!-- <i class="fas fa-4x fa-globe text-primary mb-4"></i> -->
                        <h3 class="h4 mb-2">¿Dónde comer?</h3>
                        <p class="text-muted mb-0">En nuestras instalaciones poseemos un amplio restaurante con gran variedad de platillos típicos, además tenemos un area de
                            bar donde brindamos diferentes bebidas y cocteles.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="mt-5">
                        <!--<i class="fas fa-4x fa-heart text-primary mb-4"></i> -->
                        <h3 class="h4 mb-2">Paquetes Turísticos</h3>
                        <p class="text-muted mb-0">Consulte por nuestros paquetes turísticos, los cuales mantenemos durante todo el año. Aprovecha y disfruta de una experiencia única!</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Portfolio-->
    <div id="portfolio">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                <div class="col-lg-4 col-sm-6">
                    <a class="portfolio-box" href="assets/img/portfolio/fullsize/send.jpg">
                        <img class="img-fluid" src="assets/img/portfolio/thumbnails/send.jpg" alt="" />
                        <div class="portfolio-box-caption">
                            <div class="project-category text-white-50">Categoria</div>
                            <div class="project-name">Senderismo</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="portfolio-box" href="assets/img/portfolio/fullsize/RAF.jpg">
                        <img class="img-fluid" src="assets/img/portfolio/thumbnails/RAF.jpg" alt="" />
                        <div class="portfolio-box-caption">
                            <div class="project-category text-white-50">Categoria</div>
                            <div class="project-name">Rafting</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="portfolio-box" href="assets/img/portfolio/fullsize/KYK.jpg">
                        <img class="img-fluid" src="assets/img/portfolio/thumbnails/KYK.jpg" alt="" />
                        <div class="portfolio-box-caption">
                            <div class="project-category text-white-50">Categoria</div>
                            <div class="project-name">Kayak</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="portfolio-box" href="assets/img/portfolio/fullsize/ESC.jpg">
                        <img class="img-fluid" src="assets/img/portfolio/thumbnails/ESC.jpg" alt="" />
                        <div class="portfolio-box-caption">
                            <div class="project-category text-white-50">Categoria</div>
                            <div class="project-name">Rappel</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="portfolio-box" href="assets/img/portfolio/fullsize/BCO.jpg">
                        <img class="img-fluid" src="assets/img/portfolio/thumbnails/BCO.jpg" alt="" />
                        <div class="portfolio-box-caption">
                            <div class="project-category text-white-50">Categoria</div>
                            <div class="project-name">Buceo</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="portfolio-box" href="assets/img/portfolio/fullsize/CBL.jpg">
                        <img class="img-fluid" src="assets/img/portfolio/thumbnails/CBL.jpg" alt="" />
                        <div class="portfolio-box-caption p-3">
                            <div class="project-category text-white-50">Categoria</div>
                            <div class="project-name">Cabalgatas</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <section class="page-section bg-dark text-white">
        <div class="container text-center">
            <h2 class="mb-4"> Vive la experiencia de la aventura! Anímate</h2>
        </div>
    </section>

    <!-- Contacto-->
    <!-- <section class="page-section" id="contact">  -->
    <section id="contact" class="page-section bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class="mt-0">Contáctanos, y exploremos juntos las aventuras de Costa Rica!</h2>
                    <hr class="divider my-4" />
                    <p class="text-muted mb-5">Recordá que somos una empresa compuesta por un grupo de profesionales motivados y comprometidos a ofrecerles la mejor experiencia de aventuras, comenzando desde los hermosos ríos y playas, hasta las exuberantes montañas de Costa Rica. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                    <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                    <div>+(506) 2556-1234</div>
                </div>
                <div class="col-lg-4 mr-auto text-center">
                    <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                    <!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                    <a class="d-block" href="mailto:contact@advenventurecr.com">contact@adventurecr.com</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer-->
    <?php
    include 'footer.php';
    ?>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>