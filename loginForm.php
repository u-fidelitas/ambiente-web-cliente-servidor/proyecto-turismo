<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}

if (isset($_SESSION['user'])) {
  header('Location: index.php');
}

include 'database.php';
$connection = OpenConnection();
$msg = '';
// On Click Submit
if (isset($_POST['loginBtn'])) {
  $user = $_POST['email'];
  $pass = $_POST['pass'];

  $SQL_STATEMENT = "call SP_CheckUsuario('$user','$pass')";

  $result = $connection->query($SQL_STATEMENT);

  if ($result->num_rows == 0) {
    $msg = '1';
  } else {
    $msg = '';
    $userDB = mysqli_fetch_array($result);
    $_SESSION['user'] = [
      'Usuario' => $userDB['Usuario'],
      'IdRol' => $userDB['IdRol']
    ];
    header('Location: index.php');
  }
}

CloseConnection($connection);

?>

<form method="POST" onsubmit="return validateData();">
  <div class="form-group">
    <label for="exampleInputEmail1">Correo electrónico</label>
    <input type="email" class="form-control" id="emailInp" name="email" aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">Nunca debe compartir su contraseña con nadie.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Contraseña</label>
    <input type="password" class="form-control" name="pass" id="passInp" />
    <?php if ($msg != '') { ?>
      <small class="form-text text-danger" id="helpPassLbl">Credenciales incorrectas.</small>
    <?php } ?>
  </div>
  <button type="submit" class="btn btn-primary" name="loginBtn">Ingresar</button>
</form>