<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$admin = false;
$logged = false;

if (isset($_GET['exit'])) {
    $_SESSION = array();;
}

if (isset($_SESSION['user'])) {
    $logged = true;
    if ($_SESSION['user']['IdRol'] == 1) {
        $admin = true;
    }
}
?>
<li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php">Inicio</a></li>
<li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#about">Acerca de nosotros</a></li>
<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">Servicios</a></li>
<li class="nav-item"><a class="nav-link js-scroll-trigger" href="tours.php">Tours</a></li>
<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contacto</a></li>
<li class="nav-item text-white">|</li>
<?php if (!$logged) { ?>
    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="login.php">Ingresar</a></li>
    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp.php">Registrarse</a></li>
<?php } else { ?>
    <li class="nav-item">
        <a class="nav-link disabled js-scroll-trigger">
            <?php
            echo $_SESSION['user']['Usuario'];
            if ($admin) {
            ?>
                (Administrador)
            <?php } else { ?>
                (Usuario)
            <?php } ?>
        </a>
    </li>
    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?exit=true">Salir</a></li>
<?php } ?>