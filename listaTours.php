<?php

include 'database.php';
$conn = OpenConnection();

$sql = "
SELECT 
	t.Id as tourId,
    t.Titulo,
    t.Descripcion,
    l.Provincia,
    l.Canton,
    l.Distrito,
    l.DireccionExacta,
    t.NombreContacto,
    t.EmailContacto,
    t.TelefonoContacto,
    u.Usuario,
    c.Nombre
from tours t, categorias c, usuarios u, localizaciones l 
where t.LocalizacionId= l.Id and t.IdUsuario= u.Id and t.IdCategoria= c.Id;
";

$result = $conn->query($sql);

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$admin = false;
$logged = false;

if (isset($_GET['exit'])) {
    $_SESSION = array();;
}

if (isset($_SESSION['user'])) {
    $logged = true;
    if ($_SESSION['user']['IdRol'] == 1) {
        $admin = true;
    }
}

CloseConnection($conn);
?>

<?php
while ($row = mysqli_fetch_array($result)) {
?>
    <div class="card mb-4">
        <div class="card-header text-left">
            <h4> <?php echo $row['Titulo']; ?> </h4>
        </div>
        <div class="card-body">
            <p>
                <?php echo $row['Descripcion']; ?>
            </p>
            <strong> Localización: </strong>
            <label><?php echo $row['Provincia'] . ', ' . $row['Canton'] . ', ' . $row['Distrito'] . '. ' . $row['DireccionExacta']; ?></label>
            <br>
            <label><?php echo $row['NombreContacto']; ?></label>
            <label>/</label>
            <LABEL><?php echo $row['EmailContacto']; ?></LABEL>
            <label>/</label>
            <label><?php echo $row['TelefonoContacto']; ?></label>
            <br>
            <strong> Creado por:</strong>
            <label><?php echo $row['Usuario']; ?></label>
            <br>
            <strong> Categoria:</strong>
            <label><?php echo $row['Nombre']; ?></label>
            <br>
            <br>
            <a href="tel:<?php echo $row['TelefonoContacto'] ?>" class="btn btn-primary"> <i class="fas fa-phone-volume"></i> Llamar </a>
            <?php if ($admin) { ?>
                <a href="borrarTour.php?id=<?php echo $row['tourId'] ?>" class="btn btn-danger">Borrar</a>
                <a href="agregarTour.php?id=<?php echo $row['tourId'] ?>" class="btn btn-info">Editar</a>
            <?php } ?>
        </div>
    </div>
<?php
}
?>