<?php
include("database.php");
$con = OpenConnection();
if (isset($_POST['Tours'])) {

	$Titulo = mysqli_real_escape_string($con, (strip_tags($_POST['Titulo'], ENT_QUOTES)));
	$Descripcion = mysqli_real_escape_string($con, (strip_tags($_POST["Descripcion"], ENT_QUOTES)));
	$LocalizacionId = mysqli_real_escape_string($con, (strip_tags($_POST["LocalizacionId"], ENT_QUOTES)));
	$NombreContacto = mysqli_real_escape_string($con, (strip_tags($_POST["NombreContacto"], ENT_QUOTES)));
	$EmailContacto = mysqli_real_escape_string($con, (strip_tags($_POST["EmailContacto"], ENT_QUOTES)));
	$TelefonoContacto	= mysqli_real_escape_string($con, (strip_tags($_POST["TelefonoContacto"], ENT_QUOTES)));
	$IdUsuario	= mysqli_real_escape_string($con, (strip_tags($_POST["IdUsuario"], ENT_QUOTES)));
	$IdCategoria	= mysqli_real_escape_string($con, (strip_tags($_POST["IdCategoria"], ENT_QUOTES)));

	$sql = "INSERT INTO `tours` (`Id`, `Titulo`, `Descripcion`, `LocalizacionId`, `NombreContacto`, `EmailContacto`, `TelefonoContacto`, `IdUsuario`, `IdCategoria`) 
	VALUES (NULL,'$Titulo', '$Descripcion', '$LocalizacionId', '$NombreContacto', '$EmailContacto', '$TelefonoContacto', '$IdUsuario', '$IdCategoria')";
		$insert = mysqli_query($con,$sql);

		if ($insert) {
			echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos del tour han sido guardados con éxito.</div>';
		} else {
			echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se guardaron los datos, inténtalo de nuevo !</div>';
		}
	}
	CloseConnection($con);

?>

<form class="form-horizontal" action="" method="post">
	<div class="form-group">
		<label class="col-sm-3 control-label">Título</label>
		<div class="col-sm-2">
			<input type="text" name="Titulo" value="" class="form-control" placeholder="Título" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Descripción</label>
		<div class="col-sm-4">
			<input type="text" name="Descripcion" value="" class="form-control" placeholder="Descripcion" required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">LocalizacionId</label>
		<div class="col-sm-2">
			<input type="text" name="LocalizacionId" class="form-control" placeholder="LocalizacionId">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">NombreContacto</label>
		<div class="col-sm-4">
			<input type="text" name="NombreContacto" value="" class="form-control" placeholder="NombreContacto" >
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">EmailContacto</label>
		<div class="col-sm-3">
			<input type="text" name="EmailContacto" value="" class="form-control" placeholder="EmailContacto" >
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">TelefonoContacto</label>
		<div class="col-sm-2">
			<input type="text" name="TelefonoContacto" class="form-control" placeholder="TelefonoContacto" >
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">IdUsuario</label>
		<div class="col-sm-2">
			<input type="text" name="IdUsuario" class="form-control" placeholder="IdUsuario" required>
		</div>

		<div class="form-group">
		<label class="col-sm-3 control-label">IdCategoria</label>
		<div class="col-sm-4">
			<input type="text" name="IdCategoria" value="" class="form-control" placeholder="IdCategoria" required>
		</div>
	</div>
	<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input  type="submit" name="Tours" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="tours.php" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
</form>