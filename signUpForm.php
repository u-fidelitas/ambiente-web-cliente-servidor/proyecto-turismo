<?php

include 'database.php';

$connection = OpenConnection();

// On Click Submit
if (isset($_POST['signUp'])) {
  $user = $_POST['email'];
  $pass = $_POST['pass'];

  $SQL_STATEMENT = "call SP_InsertUsuario('$user','$pass',2)";

  $connection->next_result();

  if ($connection->query($SQL_STATEMENT)) {
    header('Location: login.php');
  } else {
    echo $conexion->error;
  }
}

CloseConnection($connection);

?>

<form method="POST" onsubmit="return validateData();">
  <div class="form-group">
    <label for="exampleInputEmail1">Correo electrónico</label>
    <input type="email" class="form-control" id="emailInp" name="email" aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">Nunca debe compartir su contraseña con nadie.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Contraseña</label>
    <input type="password" class="form-control" onchange="validatePassword" name="pass" id="passInp" />
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Confirmar contraseña</label>
    <input type="password" class="form-control" id="confirmPassInp" oninput="validatePassword();" />
    <small class="form-text text-danger" id="helpPassLbl">Las contraseñas deben coincidir.</small>
  </div>
  <button type="submit" class="btn btn-primary" name="signUp">Registrarse</button>
</form>

<script>
  document.getElementById("helpPassLbl").hidden = true;

  function validatePassword() {
    let password = document.getElementById("passInp").value;
    let passwordConf = document.getElementById("confirmPassInp").value;
    if (password === passwordConf) {
      document.getElementById("helpPassLbl").hidden = true;
    } else {
      document.getElementById("helpPassLbl").hidden = false;
    }
  }
</script>