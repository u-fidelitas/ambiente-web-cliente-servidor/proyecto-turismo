SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `Tours`;
DROP TABLE IF EXISTS `Categorias`;
DROP TABLE IF EXISTS `Localizaciones`;
DROP TABLE IF EXISTS `Usuarios`;
DROP TABLE IF EXISTS `Roles`;
DROP TABLE IF EXISTS `Comentarios`;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `Tours` (
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Titulo` VARCHAR(128) NOT NULL,
    `Descripcion` TEXT NOT NULL,
    `LocalizacionId` INTEGER NULL,
    `NombreContacto` VARCHAR(256) NULL,
    `EmailContacto` VARCHAR(140) NULL,
    `TelefonoContacto` VARCHAR(15) NULL,
    `IdUsuario` INTEGER NULL,
    `IdCategoria` INTEGER NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Categorias` (
	`Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Nombre` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Localizaciones` (
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Provincia` VARCHAR(20) NOT NULL,
    `Canton` VARCHAR(100) NOT NULL,
    `Distrito` VARCHAR(100) NOT NULL,
    `DireccionExacta` TEXT NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Usuarios` (
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Usuario` VARCHAR(128) NOT NULL UNIQUE,
    `Pass` VARCHAR(32) NOT NULL,
    `IdRol` INTEGER NOT NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Roles` (
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Nombre` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Comentarios` (
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Comentario` VARCHAR(512) NOT NULL,
    `IdUsuario` INTEGER NULL,
    `IdTour` INTEGER NOT NULL,
    PRIMARY KEY (`Id`)
);

ALTER TABLE `Tours` ADD FOREIGN KEY (`LocalizacionId`) REFERENCES `Localizaciones`(`Id`);
ALTER TABLE `Tours` ADD FOREIGN KEY (`IdUsuario`) REFERENCES `Usuarios`(`Id`);
ALTER TABLE `Tours` ADD FOREIGN KEY (`IdCategoria`) REFERENCES `Categorias`(`Id`);
ALTER TABLE `Usuarios` ADD FOREIGN KEY (`IdRol`) REFERENCES `Roles`(`Id`);
ALTER TABLE `Comentarios` ADD FOREIGN KEY (`IdUsuario`) REFERENCES `Usuarios`(`Id`);