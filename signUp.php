<?php
include 'head.php';
?>

<body id="page-top">
    <!-- Navigation-->
    <?php
    include 'menuSecondary.php';
    ?>
    <!-- Masthead-->
    <header class="masthead-login">
        <div class="container h-75">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-10 align-self-end">
                    <h2 class="text-uppercase font-weight-bold">Adventure Costa Rica | Registrarse</h2>
                    <hr class="divider my-4" />
                </div>
                <div class="col-lg-4 align-self-baseline">
                    <?php
                    include 'signUpForm.php';
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </header>
    <!-- Footer-->
    <?php
    include 'footer.php';
    ?>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>